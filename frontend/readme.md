# Website Frontend

Project setup with installation of all dependencies:

```
yarn install
```

Compiles and hot-reloads for development:

```
yarn serve
```

Compiles and minifies for production:

```
yarn build
```

Lints and fixes files:

```
yarn lint
```

For more customization and configuration see [Configuration Reference](https://cli.vuejs.org/config/).
