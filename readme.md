# Initial Project and Environment Setup

Before we start developing of our website we need to do some initial work to prepare our environment and to create the website project.

## Create Backend Application

**We want to install Symfony with API-Platform for the server side backend application.**

Before we can start install required System parts:

1. Install PHP
2. Install Composer
2. Install Symfony

    ```bash
    wget https://get.symfony.com/cli/installer -O - | bash
    sudo mv ~/.symfony/bin/symfony /usr/local/bin/symfony
    ```

Create project directory and enter the directory for further steps:

```bash
mkdir -p /var/www/website
cd /var/www/website
```

Because we want to create a website we use the `website-skeleton` but this is too much for our purpose! At the beginning there many parts we don't need now.

You can find more recipes under [https://flex.symfony.com/](https://flex.symfony.com/).

Create a new website project:

```bash
composer create-project symfony/website-skeleton backend
```

We create from the beginning a small backend:

```bash
composer create-project symfony/skeleton backend
```

_Taken from [https://symfony.com/doc/master/quick_tour/the_big_picture.html](https://symfony.com/doc/master/quick_tour/the_big_picture.html)._

Now change to the `backend` application directory and serve it:

```bash
cd backend
symfony server:start
# or the shorter command
smyfony serve
```

_**Note:** if you have multiple versions installed you can specify the version with `echo 7.4 > .php-version`.

To get the latest version we change the `composer.json` and run `composer update`

```json
{
    "minimum-stability": "dev",
    "require": {
        "symfony/console": "^5.2",
        "symfony/dotenv": "^5.2",
        "symfony/flex": "^1.9",
        "symfony/framework-bundle": "^5.2",
        "symfony/yaml": "^5.2"
    }
}
```

In the next step we install `api`:

```bash
composer require api
```

## Install helpful tools

The profiler to get insides of the application specially for testing and debugging:

```bash
composer require profiler
```

For code quality PHPUnit testing framework:

```bash
composer require --dev symfony/phpunit-bridge
```

PHPStan symfony framework extensions and rules:

```bash
composer require --dev phpstan/phpstan-symfony
composer require --dev thecodingmachine/phpstan-safe-rule
composer require --dev thecodingmachine/phpstan-strict-rules
```

PHP Coding Standards Fixer Symfony Framework extensions and rules:

```bash
composer require --dev friendsofphp/php-cs-fixer
```

_**Note:** please check the latest version on [https://packagist.org/](https://packagist.org/) and update your `composer.json` before continue._

## Create Frontend

Before we can start we need to install required System parts:

1. Install [Node.js](https://nodejs.org/) incl. [NPM](https://www.npmjs.com/)
1. Install [Vue CLI](https://cli.vuejs.org/guide/installation.html)

    ```bash
    npm install -g @vue/cli
    # OR
    yarn global add @vue/cli
    ```

    For more details consider the documentation: [https://cli.vuejs.org/guide/installation.html](https://cli.vuejs.org/guide/installation.html)

1. Install Symfony encore [https://symfony.com/doc/current/frontend/encore/installation.html](https://symfony.com/doc/current/frontend/encore/installation.html):

    ```bash
    composer require symfony/webpack-encore-bundle
    ```

Now a new app can be created by adding all required dependencies for webpack encore and Vue:

```bash
yarn add --dev @symfony/webpack-encore
yarn add --dev webpack-notifier
yarn add --dev vue vue-loader vue-template-compiler
yarn add vue bootstrap-vue bootstrap
```

After this all dependencies can be installed by Yarn:

```bash
yarn install
```

To upgrade all Yarn dependencies:

```bash
yarn upgrade
```

Serve the Vue app in dev mode:

```bash
yarn run encore dev --watch
```

Hot Module Replacement (HMR), the vue-loader supports hot module replacement: just update your code and watch your Vue.js app update without a browser refresh! To activate it, use the dev-server with the --hot option:

```bash
yarn encore dev-server --hot
```

### Vue Components

For many default and well defined tasks we use following components:

* Pre Loader component for Vue.js - [https://vuejsexamples.com/pre-loader-components-for-vue-js/](https://vuejsexamples.com/pre-loader-components-for-vue-js/)

    Adjust the settings using the playground options: [https://vue-preloader.netlify.app/](https://vue-preloader.netlify.app/)

### Dedicated Vue Frontend

In the first step we want to create a frontend application:

```bash
vue create frontend
```

After this we can change to the frontend application and serve it:

```bash
cd frontend
yarn serve
```

## GIT setup

It is recommended to use git and branches for development. To get everything managed `git flow` is used inside our project.

At first install [GIT Flow](https://nvie.com/posts/a-successful-git-branching-model/):

```bash
sudo apt install git git-flow
```

After the installation we can start to configure the project repository.

GIT global setup:

```bash
git config --global user.name "Firstname Lastname"
git config --global user.email "username@domain.tld"
```

Change to the project root and add the whole project with existing folders into the new repository:

```bash
git init
git remote add origin git@gitlab.com:company/website.git
git add .
git commit -m "Initial commit"
git push -u origin master
```

If the master branch was created, we can initialize git flow:

```bash
git flow init -d
```
