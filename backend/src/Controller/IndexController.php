<?php
declare(strict_types=1);

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("", name="startpage")
 */
class IndexController extends AbstractController
{
    public function __invoke()
    {
        return $this->render('pages/index.html.twig', []);
    }
}
