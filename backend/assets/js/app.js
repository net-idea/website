/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.css';

import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue'
import loader from "vue-ui-preloader";
import Example from './components/Example'

import "bootstrap-vue/dist/bootstrap-vue.css";

Vue.use(loader);
Vue.use(BootstrapVue);

/**
 * Create a fresh Vue Application instance
 */
new Vue({
  el: '#app',
  components: {
    loader: loader,
    Example
  }
});
